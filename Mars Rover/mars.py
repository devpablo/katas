# from moves import *


INITIAL_LANDING = [[0,0],'S']
class Rover:

    def __init__(self, position = INITIAL_LANDING):
        self.coordinates = position[0]
        self.orientation = position[1]

    def actual_position(self):
        return [self.coordinates, self.orientation]

    def split_order(self, orders):

        for order in list(orders):
            self.order(order)

    def order(self, command):
        instruction = {
            'R': self.__to_the_right,
            'L': self.__to_the_left,
            'M': self.__forward
        }
        instruction[command]()

    def __to_the_right(self):
        orientation = self.orientation

        if orientation == 'N':
            self.orientation = 'E'

        if orientation == 'E':
            self.orientation = 'S'

        if orientation == 'S':
            self.orientation = 'W'

        if orientation == 'W':
            self.orientation = 'N'

    def __to_the_left(self):
        orientation = self.orientation

        if orientation == 'N':
            self.orientation = 'W'

        if orientation == 'W':
            self.orientation = 'S'

        if orientation == 'S':
            self.orientation = 'E'

        if orientation == 'E':
            self.orientation = 'N'

    def __forward(self):
        x_coordinate = self.coordinates[0]
        y_coordinate = self.coordinates[1]
        orientation = self.orientation

        if orientation == 'N':
            y_coordinate = y_coordinate + 1

        if orientation == 'E':
            x_coordinate = x_coordinate + 1

        if orientation == 'S':
            y_coordinate = y_coordinate - 1

        if orientation == 'W':
            x_coordinate = x_coordinate - 1

        self.coordinates = [x_coordinate, y_coordinate]
