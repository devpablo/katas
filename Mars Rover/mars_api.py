from flask import Flask, request
from mars import Rover

app = Flask(__name__)

app.position = [[0,0],'S']

@app.route('/position/')
def position():
   rover = Rover(app.position)
   coordinates, orientation = rover.actual_position()
   result = {'current_position':
               {
                  'x':coordinates[0],
                  'y':coordinates[1],
                  'orientation':orientation
               }
            }
   return result

@app.route('/move/', methods = ['POST'])
def move():
   order = request.json['order']

   rover = Rover(app.position)
   current = rover.actual_position()
   rover.split_order(order)
   next = rover.actual_position()
   app.position = rover.actual_position()
   
   return {
      'current_position': {
         'x':current[0][0],
         'y':current[0][1],
         'orientation': current[1]
      },
      'next_position': {
         'x': next[0][0],
         'y': next[0][1],
         'orientation': next[1]
      }
   }


if __name__ == '__main__':
    app.run()

