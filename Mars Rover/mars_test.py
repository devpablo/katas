from mamba import description, it, context
from expects import expect, equal
from mars import Rover


with description('NASA Center') as self:
    with it('checks if Mars Rover has landing'):

        rover = Rover()

        position = rover.actual_position()

        expect(position).to(equal([[0,0],'S']))

    with it('checks that Mars Rover has landing in a different point'):

        default_position = [[1,1],'N']
        rover = Rover(default_position)

        position = rover.actual_position()

        expect(position).to(equal([[1,1],'N']))

    with it('orders the Mars Rover to turn to the RIGHT'):

        rover = Rover([[0,0],'N'])

        rover.order('R')

        position = rover.actual_position()

        expect(position).to(equal([[0,0],'E']))

    with it('orders the Mars Rover to turn to the LEFT'):

        rover = Rover([[0,0],'N'])

        rover.order('L')
        position = rover.actual_position()

        expect(position).to(equal([[0,0],'W']))

    with it('orders the Mars Rover to move forward from landing position'):

        rover = Rover([[0,0],'N'])

        rover.order('M')
        position = rover.actual_position()

        expect(position).to(equal([[0,1],'N']))

    with it('orders the Mars Rover to move forward several times same orientation'):

        rover = Rover([[0,0],'N'])

        rover.split_order('MM')
        position = rover.actual_position()

        expect(position).to(equal([[0,2],'N']))

    with it('orders the Mars to turn and move in the y axis'):

        rover = Rover([[0,0],'N'])

        rover.split_order('MRMRMRM')
        position = rover.actual_position()

        expect(position).to(equal([[0,0],'W']))

