from mamba import description, it
from expects import expect, equal
import requests
from flask import Flask
from mars import Rover


with description('Mars API checks') as self:
    with it('endpoint position exists and returns a 200'):
        url = 'http://localhost:5000/position/'

        response = requests.get(url)

        expect(response.status_code).to(equal(200))

    with it('current_position and returns coordinates and orientation'):
        url = 'http://localhost:5000/position/'
        current_position = {"current_position": { "x": 0, "y": 0, "orientation": "S"}}

        response = requests.get(url)
        body = response.json()

        expect(body).to(equal(current_position))

    with it('that after receiving command, answers back its actual and final position'):

        url = 'http://localhost:5000/move/'
        order = 'MRML'
        expected_response = {
            "current_position": { "x": 0, "y": 0, "orientation": "S"},
            "next_position": { "x": -1, "y": -1, "orientation": "S"}
        }

        response = requests.post(url, json={'order': order})
        body = response.json()

        expect(body).to(equal(expected_response))

