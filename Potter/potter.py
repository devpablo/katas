SINGLE_BOOK_PRICE = 8
NO_PRICE = 0
TWO_BOOKS_DISCOUNT = 0.05
THREE_BOOKS_DISCOUNT = 0.1
FOUR_BOOKS_DISCOUNT = 0.2
FIVE_BOOKS_DISCOUNT = 0.25


def calculate_price(books):

    discount = calculate_discount(books)

    total_price = SINGLE_BOOK_PRICE * len(books)

    return total_price - discount


def calculate_discount(books):

    unique_books = len(set(books))
    base_price = SINGLE_BOOK_PRICE * unique_books
    discount = 0

    if unique_books == 2:
        discount = base_price * TWO_BOOKS_DISCOUNT

    if unique_books == 3:
        discount = base_price * THREE_BOOKS_DISCOUNT

    if unique_books == 4:
        discount = base_price * FOUR_BOOKS_DISCOUNT

    if unique_books == 5:
        discount = base_price * FIVE_BOOKS_DISCOUNT

    return discount


def book_combos(books):
    pass
