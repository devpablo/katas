from mamba import description, it, context
from expects import expect, equal
from potter import calculate_price

with description('Kata Potter') as self:
    with it('No book, no fun'):
        # Arrange
        books = []
        # Act
        price = calculate_price(books)
        # Assert
        expect(price).to(equal(0))

    with it('One book has no discounts'):
        base_price = 8
        books = ['book_one']

        price = calculate_price(books)

        expect(price).to(equal(base_price))

    with it('Two equal books has no discount'):
        base_price = 8 + 8
        books = ['book_one', 'book_one']

        price = calculate_price(books)

        expect(price).to(equal(base_price))

    with it('Two different books has a discount'):
        books = ['book_one', 'book_two']
        base_price = 8 + 8
        two_books_discount = 0.05
        discount = base_price * two_books_discount

        price = calculate_price(books)

        expect(price).to(equal(base_price - discount))

    with it('Three different books has a discount'):
        books = ['book_one', 'book_two', 'book_three']
        base_price = 8 * 3
        three_books_discount = 0.1
        discount = base_price * three_books_discount

        price = calculate_price(books)

        expect(price).to(equal(base_price - discount))

    with it('Four different books has a discount'):
        books = ['book_one', 'book_two', 'book_three', 'book_four']
        base_price = 8 * 4
        four_books_discount = 0.2
        discount = base_price * four_books_discount

        price = calculate_price(books)

        expect(price).to(equal(base_price - discount))

    with it('Five different books has a discount'):
        books = ['book_one', 'book_two', 'book_three', 'book_four', 'book_five']
        base_price = 8 * 5
        five_books_discount = 0.25
        discount = base_price * five_books_discount

        price = calculate_price(books)

        expect(price).to(equal(base_price - discount))

    with it('Four books with a three book discount'):
        books = ['book_one', 'book_two', 'book_three', 'book_two']
        base_price = 8 * 3
        three_books_discount = 0.1
        discount = base_price * three_books_discount

        price = calculate_price(books)

        expect(price).to(equal(base_price - discount + 8))
