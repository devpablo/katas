price = {
    'A':50,
    'B':30,
    'C':20,
    'D':15,
    }

def pricing_rules(item):
    return price.get(item, 0)
