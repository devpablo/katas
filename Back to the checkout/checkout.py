class Checkout():
    def __init__(self, pricing_rules):
        self.pricing_rules = pricing_rules
        self.scanned_products = []

    def scan(self, product):
        self.scanned_products.append(product)

    def total_price(self):
        price = 0
        discount_A = 20
        discount_B = 15

        for product in self.scanned_products:
            price += self.pricing_rules(product)

        if self.scanned_products.count('B') >= 2:
                amount_B = self.scanned_products.count('B') // 2
                price = price - (discount_B * amount_B)

        if self.scanned_products.count('A') >=3:
                amount_A = self.scanned_products.count('A') // 3
                price = price - (discount_A * amount_A)

        return price
