from mamba import description, it, context
from expects import expect, equal
from checkout import Checkout
from pricing_rules import pricing_rules


with description('Checkout') as self:

    with it ('Returns the price when no products are scanned'):

        products = ''
        checkout = Checkout(pricing_rules)
        checkout.scan(products)

        price = checkout.total_price()

        expect(price).to(equal(0))

    with it ('Returns the price when one "C" type product is scanned'):

        product = "C"
        checkout = Checkout(pricing_rules)
        checkout.scan(product)

        price = checkout.total_price()

        expect(price).to(equal(20))

    with it ('Returns the price when one "D" type product is scanned'):

        product = "D"
        checkout = Checkout(pricing_rules)
        checkout.scan(product)

        price = checkout.total_price()

        expect(price).to(equal(15))

    with it ('Returns the price when one "A" type product is scanned'):

        products = "A"
        checkout = Checkout(pricing_rules)
        checkout.scan(products)

        price = checkout.total_price()

        expect(price).to(equal(50))

    with it ('Returns the price when one "B" type product is scanned'):

        products = "B"
        checkout = Checkout(pricing_rules)
        checkout.scan(products)

        price = checkout.total_price()

        expect(price).to(equal(30))

    with it ('Returns the price when scans a list of different products with none more than once'):

        checkout = Checkout(pricing_rules)
        checkout.scan("C")
        checkout.scan("D")
        checkout.scan("B")
        checkout.scan("A")

        price = checkout.total_price()

        expect(price).to(equal(115))

    with it ('Returns the price when scans a list of same products with one discount'):

        checkout = Checkout(pricing_rules)
        checkout.scan("B")
        checkout.scan("B")

        price = checkout.total_price()

        expect(price).to(equal(45))

    with it ('Returns the price when scans a list of different products with one discount'):

        checkout = Checkout(pricing_rules)
        checkout.scan("B")
        checkout.scan("A")
        checkout.scan("B")

        price = checkout.total_price()

        expect(price).to(equal(95))

    with it ('2 B discount'):

        checkout = Checkout(pricing_rules)
        checkout.scan("B")
        checkout.scan("C")
        checkout.scan("B")
        checkout.scan("A")
        checkout.scan("B")
        checkout.scan("B")

        price = checkout.total_price()

        expect(price).to(equal(160))

    with it ('3 A discount'):

        checkout = Checkout(pricing_rules)
        checkout.scan("A")
        checkout.scan("C")
        checkout.scan("A")
        checkout.scan("A")
        checkout.scan("A")
        checkout.scan("A")

        price = checkout.total_price()

        expect(price).to(equal(250))

    with it ('A and B discounts'):

        checkout = Checkout(pricing_rules)
        checkout.scan("A")
        checkout.scan("C")
        checkout.scan("A")
        checkout.scan("A")
        checkout.scan("A")
        checkout.scan("A")
        checkout.scan("A")
        checkout.scan("B")
        checkout.scan("D")
        checkout.scan("B")
        checkout.scan("D")
        checkout.scan("B")
        checkout.scan("B")
        checkout.scan("B")
        checkout.scan("A")



        price = checkout.total_price()

        expect(price).to(equal(480))
