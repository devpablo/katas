# Katas

All katas are made using Python 3 and Mamba as test suite. For more info refer to:

[Python 3](https://docs.python.org/3/)

[Mamba](http://nestorsalceda.com/mamba/)


## How to run tests

Use the command:

    pipenv run mamba test_file.py --format=documentation


## Potter

<http://codingdojo.org/kata/Potter/>


## Back to the Checkout

<http://codekata.com/kata/kata09-back-to-the-checkout/>